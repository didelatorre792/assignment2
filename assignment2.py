from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template("showBook.html", books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_book_t= request.form["NewBook"]
        books.append({"title": new_book_t , "id": str(len(books) + 1)})
        return redirect(url_for("showBook"))
    return render_template("newBook.html")

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    book = books[book_id - 1]
    if request.method == 'POST': 
        new_title = request.form["bookN"]
        books[book_id - 1]["title"] = new_title
        return redirect(url_for("showBook"))
    return render_template("editBook.html", title = book["title"], id = book["id"])
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    book = books[book_id - 1] 
    if request.method == 'POST':
        books.pop(book_id - 1)
        for item in range(len(books)):
            books[item]["id"] = item + 1
        return redirect(url_for("showBook"))
    return render_template('deleteBook.html', title = book['title'], id = book_id)
        
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

